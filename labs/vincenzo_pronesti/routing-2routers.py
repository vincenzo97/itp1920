from mininet.net import Mininet
from mininet.node import Controller, RemoteController, OVSController
from mininet.node import CPULimitedHost, Host, Node
from mininet.node import OVSKernelSwitch, UserSwitch
from mininet.node import IVSSwitch
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.link import TCLink, Intf
from subprocess import call


def myNetwork():
    """

          eth0         eth0   eth1           eth0  eth1     eth0
        h1 --------------- r1 ------------------ r2 ----------- h2
    10.0.1.10     10.0.1.1   10.0.2.10   10.0.2.1  10.0.3.10    10.0.3.1

    """
    net = Mininet(topo=None, build=False)
    info('*** Adding controller\n')
    info('*** Add routers\n')
    r1 = net.addHost('r1', cls=Node, ip=None)
    r2 = net.addHost('r2', cls=Node, ip=None)

    info('*** Adding hosts\n')
    h1 = net.addHost('h1', cls=Host, ip=None, mac='00:00:10:00:00:64')
    h2 = net.addHost('h2', cls=Host, ip=None, mac='00:00:10:00:01:64')

    info('*** Add links\n')
    net.addLink(h1, r1)
    net.addLink(r1, r2)
    net.addLink(h2, r2)

    info('*** Starting network\n')
    net.build()

    info('*** Starting controllers\n')
    for controller in net.controllers:
        controller.start()

    info('*** Starting switches\n')
    r1.intf('r1-eth0').config(mac='00:00:10:00:00:01')
    r1.intf('r1-eth1').config(mac='00:00:10:00:01:01')
    r2.intf('r2-eth0').config(mac='00:00:10:00:02:01')
    r2.intf('r2-eth1').config(mac='00:00:10:00:03:01')

    info('*** Turn off IPv6\n')
    r1.cmd('sysctl -w net.ipv6.conf.all.disable_ipv6=1')
    r1.cmd('sysctl -w net.ipv6.conf.default.disable_ipv6=1')
    r2.cmd('sysctl -w net.ipv6.conf.all.disable_ipv6=1')
    r2.cmd('sysctl -w net.ipv6.conf.default.disable_ipv6=1')
    h1.cmd('sysctl -w net.ipv6.conf.all.disable_ipv6=1')
    h1.cmd('sysctl -w net.ipv6.conf.default.disable_ipv6=1')
    h2.cmd('sysctl -w net.ipv6.conf.all.disable_ipv6=1')
    h2.cmd('sysctl -w net.ipv6.conf.default.disable_ipv6=1')

    info('*** Post configure routers and hosts\n')
    h1.cmd('ip a add 10.0.1.10/24 dev h1-eth0')
    h1.cmd('ip route add default via 10.0.1.1 dev h1-eth0')
    h1.cmd('ip link set h1-eth0 up')

    h2.cmd('ip a add 10.0.3.1/24 dev h2-eth0')
    h2.cmd('ip route add default via 10.0.3.10 dev h2-eth0')
    h2.cmd('ip link set h2-eth0 up')

    r1.cmd('sysctl -w net.ipv4.ip_forward=1')
    r1.cmd('ip a add 10.0.1.1/24 dev r1-eth0')
    r1.cmd('ip link set r1-eth0 up')
    r1.cmd('ip a add 10.0.2.10/24 dev r1-eth1')
    r1.cmd('ip link set r1-eth1 up')
    r1.cmd('ip route add 10.0.3.0/24 via 10.0.2.1 dev r1-eth1')

    r2.cmd('sysctl -w net.ipv4.ip_forward=1')
    r2.cmd('ip a add 10.0.2.1/24 dev r2-eth0')
    r2.cmd('ip link set r2-eth0 up')
    r2.cmd('ip a add 10.0.3.10/24 dev r2-eth1')
    r2.cmd('ip link set r2-eth1 up')
    r2.cmd('ip route add 10.0.1.0/24 via 10.0.2.10 dev r2-eth0')

    CLI(net)
    net.stop()


if __name__ == '__main__':
    setLogLevel('info')
    myNetwork()

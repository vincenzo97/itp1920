#!/usr/bin/python

from mininet.net import Mininet
from mininet.node import Controller, RemoteController, OVSController
from mininet.node import CPULimitedHost, Host, Node
from mininet.node import OVSKernelSwitch, UserSwitch
from mininet.node import IVSSwitch
from mininet.cli import CLI
from mininet.log import setLogLevel, info
from mininet.link import Link, TCLink, Intf
from subprocess import call

def myNetwork():
	"""
				10.0.0.2         10.0.0.5
				   h2 				h5
				   |eth0			|eth0
				   |				|
				   |				|
				   |				|
			  eth1 |				|eth1
	eth0	 eth0  | eth2			|  eth2       eth0     
	h1 ---------- s1 -------------- s2 ------------ h4
	10.0.0.1	   |		   eth0		         10.0.0.4
				   | eth3
				   |
				   |
				   |eth0
				   h3
		        10.0.0.3

	"""

	net = Mininet( topo=None,
			       build=False )


	info( '*** Add switches\n')
	s1 = net.addHost('s1', cls=Node, ip=None)
	s2 = net.addHost('s2', cls=Node, ip=None)

	info( '*** Add hosts\n')
	h1 = net.addHost('h1', cls=Host, mac='00:00:10:10:00:01')
	h2 = net.addHost('h2', cls=Host, mac='00:00:10:10:00:02')
	h3 = net.addHost('h3', cls=Host, mac='00:00:10:10:00:03')
	h4 = net.addHost('h4', cls=Host, mac='00:00:10:10:00:04')
	h5 = net.addHost('h5', cls=Host, mac='00:00:10:10:00:05')


	info( '*** Add links\n')
	Link(s1, h1, intfName1='s1-eth0', intfName2='h1-eth0')
	Link(s1, h2, intfName1='s1-eth1', intfName2='h2-eth0')
	Link(s1, h3, intfName1='s1-eth3', intfName2='h3-eth0')
	Link(s2, h4, intfName1='s2-eth2', intfName2='h4-eth0')
	Link(s2, h5, intfName1='s2-eth1', intfName2='h5-eth0')
	Link(s1, s2, intfName1='s1-eth2', intfName2='s2-eth0')


	info( '*** Starting network\n')
	net.build()
	info( '*** Starting controllers\n')
	for controller in net.controllers:
		controller.start()

	info( '*** Starting switches\n')

	info('*** Turn off IPv6\n')
	h1.cmd('sysctl -w net.ipv6.conf.all.disable_ipv6=1')
	h1.cmd('sysctl -w net.ipv6.conf.default.disable_ipv6=1')
	h2.cmd('sysctl -w net.ipv6.conf.all.disable_ipv6=1')
	h2.cmd('sysctl -w net.ipv6.conf.default.disable_ipv6=1')
	h3.cmd('sysctl -w net.ipv6.conf.all.disable_ipv6=1')
	h3.cmd('sysctl -w net.ipv6.conf.default.disable_ipv6=1')
	h4.cmd('sysctl -w net.ipv6.conf.all.disable_ipv6=1')
	h4.cmd('sysctl -w net.ipv6.conf.default.disable_ipv6=1')
	h5.cmd('sysctl -w net.ipv6.conf.all.disable_ipv6=1')
	h5.cmd('sysctl -w net.ipv6.conf.default.disable_ipv6=1')


	h1.cmd('ip a add 10.0.0.1/24 dev h1-eth0')
	h1.cmd('ip link set h1-eth0 up')
	h2.cmd('ip a add 10.0.0.2/24 dev h2-eth0')
	h2.cmd('ip link set h2-eth0 up')
	h3.cmd('ip a add 10.0.0.3/24 dev h3-eth0')
	h3.cmd('ip link set h3-eth0 up')
	h4.cmd('ip a add 10.0.0.4/24 dev h4-eth0')
	h4.cmd('ip link set h4-eth0 up')
	h5.cmd('ip a add 10.0.0.5/24 dev h5-eth0')
	h5.cmd('ip link set h5-eth0 up')

	s1.cmd('brctl addbr br1')
	s1.cmd('brctl addif br1 s1-eth0')
	s1.cmd('brctl addif br1 s1-eth1')
	s1.cmd('brctl addif br1 s1-eth2')
	s1.cmd('brctl addif br1 s1-eth3')
	s1.cmd('ip link set br1 up')

	s2.cmd('brctl addbr br2')
	s2.cmd('brctl addif br2 s2-eth0')
	s2.cmd('brctl addif br2 s2-eth1')
	s2.cmd('brctl addif br2 s2-eth2')
	s2.cmd('ip link set br2 up')

	info( '*** Post configure switches and hosts\n')

	CLI(net)
	net.stop()

if __name__ == '__main__':
	setLogLevel( 'info' )
	myNetwork()
